All scripts mentioned below are in the Scripts folder

Original file: countstable2.txt

# To remove miRNAs with less than 10 reads in an individual and not present in at least one other individual
Follow the steps in DESeq2.R script

Normalised file: normalized.countstable.txt

# Quality assessment of the sequencing reads
Restrict size of sequences to 17-25 nucleotides
In FloridaData folder (work directory), there are subfolders for all the samples F1-F40.
In each sample folder, use filtered_17_to_25.fastq.gz
Unzip and move it to another file
> gunzip -c filtered_17_to_25.fastq.gz > filtered_17_to_25.fq
> module load fastx
> fastx_quality_stats -i filtered_17_to_25.fq -o filteredstats.txt
> fastq_quality_boxplot_graph.sh
> (link) -i filteredstats.txt -t F1 -p -o F1filteredquality.png
Convert .png to .jpg

For F1 to F40 filteredstats.txt, read in to R
> colMeans(F1[,3:12])
To get average value of 25 nucleotides for min,max,sum,mean,Q1,med,Q3,IQR,IW,rW
save as .csv

Using the .csv file saved previously, make a figure with boxplots in R
Read in the .csv file
Follow steps in fastx quality boxplot.R

# Quantification of types of variation
From the normalized.countstable.txt, the first column with the unique identifier of the miRNA was selected:

> awk ‘{print $1}’ normalized.countstable.txt > list.txt

After that, the “_” that connected the identifier was replaced with “tab”: 

> tr “_” “\t” < list.txt > list1.txt

A file with 10 columns was produced. 
To calculate the total types of non-template addition that occurred at 3’ end, the sixth column was selected and the types were quantified:

> awk ‘{print $6}’ list1.txt | sort | uniq | wc –l 

The frequency of each type of non-template addition was calculated:

> awk ‘{print $6}’ list1.txt | sort | uniq –c

The above two commands could be modified to calculate the types of template variations at 5’ end and 3’ end, 
which were in the seventh and eighth column respectively in list1.txt. 

# Identifying canonical-miR and iso-miR
To identify canonical-miRs in the dataset, mature.fa was downloaded from miRBase. 
The mature.fa file was then converted into a tab delimited text file, 
mature.txt using an R script (fasta2txt.R)

The mature miRNA sequences of human were selected,
commands were applied to create a file with only the miRNA name and the mature miRNA sequence. 

> grep “hsa” mature.txt > hsa_mature.txt
> sed –E ‘s/\st/\t/g’ hsa_mature.txt > hsa_mature1.txt
> cut –f1,6 hsa_mature1.txt > hsa_mature2.txt 
> cut –c 2- hsa_mature2.txt > hsa_mature3.txt
> tr ‘\t’ ‘_’ < hsa_mature3.txt | tr ‘U’ ‘T’ > hsa_mature4.txt

With the list of mature human miRNAs in hsa_mature4.txt, 
the miRNAs in normalized.countstable.txt could be compared to it, 
and the miRNAs in the dataset that matched the miRNAs in the list would be canonical-miRs, while the rest of the miRNAs would be iso-miRs.

> grep –f hsa_mature4.txt normalized.countstable.txt > canonical.txt
> grep –v –f hsa_mature4.txt normalized.countstable.txt > isomir.txt

# Renaming of miRNA
canonicallist.txt and isomirlist.txt have the full name of the miRNAs
** fulllist.txt -- list of old names and new names for translation

For canonical miRNA, keep the name (eg miR-345-3p)
> awk '{print $2,$3,$4....$41}' humcanonical.txt | tr ' ' '\t' > canonical-counts.txt
> awk '{print $1}' humcanonical.txt | tr ' ' '\t' | awk '{print $1}' | cut -c 5- > canonical-name.txt
> paste canonical-name.txt canonical-counts.txt | column -s $'\t' -t > canonical-full.txt


For isomiRs, use iso-miR-1, iso-miR-2.... iso-miR-11565
Do the above steps to get isomir-full.txt

> cat canonical-full.txt > modifieddata.txt
> cat isomir-full.txt >> modifieddata.txt

Repeat the same for vsd.txt to get modifiedvsd.txt

Separate into modified-NOcounts.txt, modified-LTcounts.txt, modified-NOvsd.txt, modified-LTvsd.txt
They are in the order of miRname--10controls--10cases
They are used in creating heatmaps. (pheatmap.R)

# Isomir splitting into Profile I and Profile II - files are in isomir_splitting folder
Profile I - Lowly expressed and frequent isomiRs
Part11: Freq >= 20, log Mean < 2
Part12: Freq < 20, log Mean < 1
Part21: Freq >= 20, log Mean >= 2
Part22: Freq < 20, log Mean >= 1

Merge Part11 and Part12 for isomiRs in Profile I, Part21 and Part2 for isomiRs in Profile II.

# Tissue specificity calculations - Z-score R script using tissue.normalized.countstable.txt
All files in Tissue specificity folder
Files for the selection of tissue specific and highly expressed miRNAs are in this folder too.

# GSEA for enrichment of beta cell- and islet-specific miRNAs in New Onset T1D samples
.gct files = canonical-miR, iso-miR, normalized.counts (all)
.grp files = beta-islet expressed, beta-islet Z>0, beta-islet Z>1, beta-islet Z>2, 
and control files (colon, heart, muscle,liver - Z>2)
.cls file = phenotype labels


# Differential expression analysis
1. NewOnset-DESeq = 10 controls vs 10 New Onset T1D
2. LongTerm-DESeq = 10 controls vs 10 Long Term T1D
3. NewOnset-3cases-DESeq= 10 controls vs 3 New Onset T1D (F21,F25,F30)
.csv files for the top 10 selected miRNAs to be tested in CombiROC are in these folders too.

# Assessing the level of beta-islet specific miRNAs in samples
(Assessment-level-sample folder)
Five groups of miRNAs - Z<-1, -1<Z<0, 0<Z<1, 1<Z<2, Z>2
To get the miRNAs and their expression in each of the Z-score group
For example: Choosing miRNAs with Z>2 and highly expressed
> awk '{print $2,$4}' betaspecificity.txt | awk '$2>=2' | grep -v "NA" > betaz2.txt
> awk '{print $2,$4}' isletspec.txt | awk '$2>=2' | grep -v "NA" > isletz2.txt
> awk '{print $1}' betaz2.txt > betaz2list.txt
> awk '{print $1}' isletz2.txt > isletz2list.txt
> cat betaz2list isletz2list.txt | sort | uniq > beta-islet-z2list.txt

> awk '{print $1}' highlyexpressedmir.txt | tr '_' '\t' | awk '{print $1"_"$3"_"$4"_"$5"_"$6"_"$7"_"$8"_"$9"_"$10"_"$2}' > highexplist.txt
> grep -Fwf highexplist.txt beta-islet-z2list.txt > wantedlist.txt


# WGCNA for New Onset and Control samples
Files from WGCNA, and also files used to investigate the Green module.
.csv for the miRs to be tested in CombiROC is in the folder too.
