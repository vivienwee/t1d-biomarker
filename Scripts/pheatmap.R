library("pheatmap")
novsd<-read.delim("NOvsdforgoldcombo.txt",header = FALSE,sep="\t",row.names = 1)
rownames(novsd)<-novsd$miRNA
exp_matrix<-as.matrix(novsd[1:20])
colData<-read.delim("NO-codes.txt",header = TRUE,sep="\t",row.names = 1)
names(novsd)[1]<-"F1"
names(novsd)[2]<-"F2"
names(novsd)[3]<-"F4"
names(novsd)[4]<-"F10"
names(novsd)[5]<-"F13"
names(novsd)[6]<-"F14"
names(novsd)[7]<-"F18"
names(novsd)[8]<-"F27"
names(novsd)[9]<-"F34"
names(novsd)[10]<-"F38"
names(novsd)[11]<-"F3"
names(novsd)[12]<-"F7"
names(novsd)[13]<-"F16"
names(novsd)[14]<-"F19"
names(novsd)[15]<-"F21"
names(novsd)[16]<-"F24"
names(novsd)[17]<-"F25"
names(novsd)[18]<-"F30"
names(novsd)[19]<-"F31"
names(novsd)[20]<-"F35"
novsd<-novsd[,rownames(colData)]
all(rownames(colData)==colnames(novsd))
annotation<-data.frame(colData)
rownames(annotation)<-colnames(novsd)
pheatmap(novsd,annotation=annotation,scale="row",cluster_cols=FALSE,cluster_rows=FALSE,main="New Onset vs Controls")

# For newonset down regulated, can't plot heatmap when add scale=row
# Because the SD of one of the miRNAs is 0.
library("matrixStats")
novsd$sd<-apply(novsd[,1:20],1,sd)
threshold=0
filtered<-subset(novsd,novsd[,21]!=threshold)
novsd<-as.data.frame(filtered)
novsd$sd<-NULL
# Run pheatmap command again